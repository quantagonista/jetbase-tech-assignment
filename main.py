# Author: quantagonista
# Version: Python 3.10.6

""" Дана последовательность чисел от 1 до 10 ... или 10 миллиардов.
   Числа отсортированы по возрастанию (1, 2, 3 и т.д.).
   В этой последовательности отсутствуют 2 числа
   - мы знаем длину и границы последовательности,
   мы знаем что в последовательности отсутствуют 2 числа,
   но не знаем - какие именно числа отсутствуют.
   Tasks :
    - Нужно найти отсутствующие числа оптимальным способом.
      например если отсутствуют 2 и 3 последовательность
      будет выглядеть следующим образом
      1, 4, 5, 6 ... 10.000.000.000
    - Нужно оценить вычислительную сложность решения. """

MAX_ARRAY_SIZE = 10e5


def find_missing_numbers(sequence: list[int], length: int) -> list[int]:
    """ Finds and returns missing 2 numbers fron given array
    :param sequence: input array of numbers
    :param length: length of the given array
    :return: list of 2 missing numbers
    """

    result = []
    sequence_index = 0

    # due to only one main loop,
    # algorithmic speed is linear - O(n)
    for index, number in enumerate(range(1, length + 1)):
        if len(result) == 2:
            break

        try:
            sequence_number = sequence[sequence_index]
            # after finding first missing numbers,
            # we need to switch pointer in array further
            if sequence_number != number:
                result.append(number)
                sequence_index -= 1

        # for cases when missing numbers are in the end of array
        except IndexError:
            result.append(number)

        sequence_index += 1

    return result


# Tests
def test_1_to_10_with_all():
    assert find_missing_numbers(list(range(1, 11)), 10) == []


def test_1_to_10_without_1_2():
    assert find_missing_numbers([3, 4, 5, 6, 7, 8, 9, 10], 10) == [1, 2]


def test_1_to_10_without_5_6():
    assert find_missing_numbers([1, 2, 3, 4, 7, 8, 9, 10], 10) == [5, 6]


def test_1_to_10_without_9_10():
    assert find_missing_numbers([1, 2, 3, 4, 5, 6, 7, 8], 10) == [9, 10]


def test_1_to_10_without_2_7():
    assert find_missing_numbers([1, 3, 4, 5, 6, 8, 9, 10], 10) == [2, 7]


def test_random_n_executions(n: int):
    """ creates random test data and executes n times with different data """
    import random

    def rand(start, end): return random.randint(start, end)

    for _ in range(n):
        random_sequence_length = rand(10, MAX_ARRAY_SIZE)
        print('Sequence Length: ', random_sequence_length)

        # create random sequence
        sequence = list(range(1, random_sequence_length + 1))

        # choose 2 random numbers to remove
        random_1_index = rand(1, random_sequence_length - 1)
        random_2_index = rand(1, random_sequence_length - 1)

        random_1 = sequence[random_1_index]
        random_2 = sequence[random_2_index]

        print('R1: ', random_1)
        print('R2: ', random_2)

        # we cannot delete same number twice
        if random_1 == random_2:
            random_2 = random_1 // 2

        sequence.remove(random_1)
        sequence.remove(random_2)

        result = sorted(find_missing_numbers(sequence, random_sequence_length))
        expected = sorted([random_1, random_2])
        assertion = sorted(result) == sorted(expected)

        print('Expected: ', expected)
        print('Result: ', result)
        print('Passed: ', assertion)

        print('')
        assert assertion


if __name__ == '__main__':
    test_1_to_10_with_all()
    test_1_to_10_without_1_2()
    test_1_to_10_without_5_6()
    test_1_to_10_without_2_7()
    test_1_to_10_without_9_10()

    test_random_n_executions(3)
